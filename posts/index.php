<? require($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Список постов</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <?
                $items_on_page = 5;
                if (isset($_GET['page'])) {
                    $page = intval($_GET['page']);
                }
                if ($page <= 0) {
                    $page = 1;
                }
                $start_page = ($page - 1) * $items_on_page;
                $query = 'SELECT p.*, u.firstname AS author_firstname, u.lastname AS author_lastname
                FROM posts p, users u
                WHERE p.author_id = u.id
                ORDER BY p.date DESC
                LIMIT ' . $start_page . ', ' . $items_on_page;
                $res = $mysql->query($query);

                $count_query = 'SELECT COUNT(*) AS count FROM posts';
                $count_res = $mysql->query($count_query);
                $count_arr = $count_res->fetch_assoc();
                $posts_count = intval($count_arr['count']);
                $pages_count = ceil($posts_count / $items_on_page);

                $posts = [];
                while($post = $res->fetch_assoc()) {
                    $posts[$post['id']] = $post;
                }

                $tags_query = 'SELECT t.text, tp.post_id
                           FROM tags t, tags_posts tp
                           WHERE t.id = tp.tag_id;';
                $tags_res = $mysql->query($tags_query);
                while($tags_data = $tags_res->fetch_assoc()) {
                    $post_id = $tags_data['post_id'];
                    if (isset($posts[$post_id])) {
                        if (!isset($posts[$post_id]['tags'])) {
                            $posts[$post_id]['tags'] = [];
                        }
                        $posts[$post_id]['tags'][] = $tags_data['text'];
                    }
                }
                //echo '<pre>';
                //var_dump($posts);
                //echo '</pre>';

                                
                if ($page > $pages_count) {
                    echo 'Страницы с этим номером не существует';
                } else {
                    foreach($posts as $post) { ?>
                        <article>
                            <h2><?= $post['title'] ?></h2>
                            <h3><?= $post['author_firstname'] ?> <?= $post['author_lastname'] ?></h3>
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-xs-6">
                                        <img src="<?= $post['preview_img'] ?>" alt="">
                                    </div>
                                    <div class="col-xs-6">
                                        <p><?= $post['preview_text'] ?></p>
                                        <p><a href="/posts/detail.php?id=<?= $post['id'] ?>">Читать далее...</a></p>
                                    </div>
                                </div>
                            </div>
                            <? if (is_array($post['tags'])) { ?>
                                <p>Теги: <?= implode(' ', $post['tags']) ?></p>
                            <? } ?>
                            <p><?= $post['date'] ?></p>
                        </article>
                    <? }
                }
                ?>
            </div>
        </div>
    </div>
<? require($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>