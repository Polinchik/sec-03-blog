<? require($_SERVER['DOCUMENT_ROOT'] . '/header.php'); ?>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <h1>Пост</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
			<? 
			 $query = 'SELECT p.*, u.firstname AS author_firstname, u.lastname AS author_lastname
						FROM posts p, users u
						WHERE p.author_id = u.id
						ORDER BY p.date DESC';
			$res = $mysql->query($query);
			$id_GET = intval($_GET['id']);
			while($post = $res->fetch_assoc()) {
				if ($id_GET == $post['id']) { ?>
					<article>
						<h2><?= $post['title'] ?></h2>
						<img src="<?= $post['preview_img'] ?>" alt="">
						<p><?= $post['content'] ?></p>
						<h3>Автор: <?= $post['author_firstname'] ?> <?= $post['author_lastname'] ?></h3>
						<p>Дата публикации: <?= $post['date'] ?></p>
					</article>
					<a href="/posts/index.php">Назад</a>
				<? }
			}
			?>
           </div>
        </div>
    </div>
<? require($_SERVER['DOCUMENT_ROOT'] . '/footer.php'); ?>